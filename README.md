# Markdown Editor

![MIT](https://img.shields.io/badge/LICENSE-MIT-ORANGE.svg?longCache=true&style=for-the-badge "MIT") 
![VUE](https://img.shields.io/badge/VUE-2.x-GREEN.svg?longCache=true&style=for-the-badge "VUE")

## DEMO

[Live demo](http://mark.kacperlukasik.pl/)


## TODO

- better ui
- toolbox
- responsive(mobile version)