import _ from 'lodash';
import Vue from 'vue';
import Markdown from './Components/MarkdownEditor.vue';

Vue.component('markdown-editor', Markdown);

const app = new Vue({
    el: '#app',
});