var Encore = require('@symfony/webpack-encore');

Encore
    .setOutputPath('./src')
    .setPublicPath('/src')
    .addEntry('appJS', './assets/js/app.js')
    .addStyleEntry('appSCSS','./assets/css/app.scss')
    .enableSassLoader()
    .autoProvidejQuery()
    .cleanupOutputBeforeBuild()
    .enableBuildNotifications()
    .enableVueLoader()
;
module.exports = Encore.getWebpackConfig();